模板文件报错的提示是这样的：

![image](./images/clipboard.png)

解决的配置项：

![image](./images/clipboard_1.png)

解决办法：

在 vscode 的 settings.json 中配置（这里配置将所有项目都会起效）：

依次点击：文件——首选项——设置，在打开的页面点击，用户——拓展——vetur
然后疯狂下滑，找到 Validation: Interpolation 一栏，看到这里有一句：
validate interpolation in `<template>` region using TypeScript language service
大意就是按照 TypeScript 的语法规则去校验`<template>`中的语句。
豁然开朗，关闭该选项即可。
至于为啥会勾选上，八成是因为 vetur 升级的原因吧 。

在某个项目的 .vscode/settings.json 中配置（这里配置将只在这个项目里起效）

![image](./images/clipboard_2.png)
