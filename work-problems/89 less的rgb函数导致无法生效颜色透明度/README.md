index.vue：


```html
<style lang="less" module>
.box{
    height: 100%;
    overflow: auto;
    background-color: rgb(250, 195, 39, 0.2);
}
</style>
```

输出为：

```css
.src-views-user--update-log_box_3QycD {
    height: 100%;
    overflow: auto;
    background-color: #fac327;
}
```

这里的 background-color 颜色被编译成了 #16禁止并且透明度设置的 0.2 也失效了。

问题的原因：
由于我们设置了 lang="less" 语言设置为了 less 编译然后 less 中的 [颜色定义函数](http://lesscss.cn/functions/#color-definition-rgb) 刚好就是名为 rgb 的函数。

解决方法：
将rgb改为rgba

```html
<style lang="less" module>
.box{
    height: 100%;
    overflow: auto;
    background-color: rgba(250, 195, 39, 0.2);
}
</style>
```

输出为：

```css
.src-views-user--update-log_box_3QycD {
    height: 100%;
    overflow: auto;
    background-color: rgba(250,195,39,.2);
}
```
