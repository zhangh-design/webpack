场景：
项目里没有开启 dev-Server 的 https: true 但是在打开网页时（不使用 localhost 访问，使用本地ip地址）所有的资源文件访问都变成了 https 协议了。

问题产生的来源：
index.hml

```html
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
```

去项目的head中查看，是否有`<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">`，果然，问题就在这里。页面一旦发现有这个请求头，就在加载 http 资源时自动替换成 https 请求的，注释此代码，即可解决啦。

