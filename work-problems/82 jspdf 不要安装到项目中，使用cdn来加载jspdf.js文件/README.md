**场景：**
项目进行生成环境的构建，打包输出dist目录，在static/js/chunk-vendors.935d32be.js文件中出现了不安全的链接（大数据局用户反馈的可能是用什么工具扫出来的）：
http://www.willow-systems.com/ 【就是这么个链接】

chunk-vendors.935d32be.js 文件由于每次构建都不同，只看 chunk-verdors 名字就行。

项目的依赖package.json：

![image](./1.png)

构建的dist目录：

![image](./2.png)


**解决：**
打开 chunk-vendors.935d32be.js 构建文件后的发现了

![image](./3.png)

这个注释中出现了 `willow-systems.com` 这个文字，旁边又发现了jsPDF这个插件的名字结合右上角出现的`e.processJPEG` 【看到这个的时候我差不多了解了项目里面用到JPEG处理的也只有 jsPDF了，将图片转pdf】，
然后我的解决方案就是将 `"jspdf": "^2.2.0"` 从package.json中删除依赖【从新安装依赖并build构建之后确实发现这个注释已经没有了，看来问题解决了】，然后使用 cdn的形式载入jspdf.js这个资源文件，然后代码里写的`import jsPDF from "jspdf";`
代码删除掉用 window.jsPDF来代替。
