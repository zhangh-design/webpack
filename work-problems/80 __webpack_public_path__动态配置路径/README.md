
#### __webpack_public_path__动态配置路径

摘至-[前端大概一分钟】__webpack_public_path__动态配置路径](https://blog.csdn.net/weixin_34004750/article/details/88012043)

<font size=4>**场景：**</font>

公司项目页面是jsp，前端开发用的是webpack + vue全家桶。项目部署时会动态配置一个相对路径


```html
<script src="<%=request.getContextPath()%>/vue-static/build-dist/manifest.js"></script>
```

webpack输出配置：


```javascript
output: {
    // 文件输出目录
    path: resolve('./build-dist'),
    // 资源加载路径
    publicPath: '/vue-static/build-dist/',
    ......
}
```

jsp页面中引入的 js、css、img 都可以通过这种方式引入，但是 publicPath 配置项是写死的，webapck编译后的js，css引入的字体、图片在引入时会缺少相对路径导致404错误。


<font size=4>**解决方法：**</font>

查看webpack文档，发现一神器[__webpack_public_path__](https://webpack.docschina.org/guides/public-path/#on-the-fly)。用一句话来解释的话，这货就是output配置中的“publicPath”参数另外一种配置方式。

1. 在jsp页面中新建变量存放相对路径

```html
<script>
    // 全局用到的信息
    var INFO_G = {
        // 上下文路径
        contextPath: '<%=request.getContextPath()%>',
        // 企业id
        entId: '${entId}',
        // 用户id
        userId: '${userId}'
    };
</script>
```

2. 修改webpack输出配置项

```javascript
output: {
    // 文件输出目录
    path: resolve('./build-dist'),
    // 资源加载路径
    publicPath: '',
    ......
}
```

3. 新建配置文件config.js，在里面添加__webpack_public_path__配置项内容

```javascript
__webpack_public_path__ = INFO_G.contextPath + '/vue-static/dist/';
```

4. 在入口文件main.js中引入配置文件（注意：一定要在最上面引入） 

```javascript
import './config' // 配置文件
import Vue from 'vue'
import App from './app.vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import router from './router'
import axios from 'axios'
```






















