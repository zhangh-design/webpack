### 配置的是一个http链接：

![image](http://m.qpic.cn/psc?/V12UXEll2JjLTU/TmEUgtj9EK6.7V8ajmQrEAHFooIiQ6GdYMqdy*50kBHgn38mwEBhR5Xr9z.5S8iuCiaBCJ9DR5343akBntxuPeAulcV6cJ*2i5hKV3Hpvqs!/b&bo=GQOoAQAAAAADJ7E!&rf=viewer_4&t=5)
![image](./images/7.png)

构建编译后的chunk资源js文件输出：

![image](http://m.qpic.cn/psc?/V12UXEll2JjLTU/TmEUgtj9EK6.7V8ajmQrEIO.0.jkEaQUZ80HTR*trNx6.UpLeNdccKutYDwf6OsXBZ3aQ5M0uhbyWB6oN9QjWssiQfNcEY2MyuTFnyvEHOs!/b&bo=uwIXAQAAAAADF50!&rf=viewer_4&t=5)
![image](./images/8.png)

### 如果把 publicPath 的配置给注释掉：

![image](http://m.qpic.cn/psc?/V12UXEll2JjLTU/TmEUgtj9EK6.7V8ajmQrEG2gHqWuL1gyjZDm58I0*nKc95mXT8SNMv3gDsYWtgd*gfyiNZ.M0OInFWoZLpTegicIfxVlZPZWJd8oR7.30pA!/b&bo=KQOiAQAAAAADJ4s!&rf=viewer_4&t=5)
![image](./images/6.png)

构建编译后的chunk资源js文件输出：

![image](http://m.qpic.cn/psc?/V12UXEll2JjLTU/TmEUgtj9EK6.7V8ajmQrEP8fHXaL8Gz782*sjiEwCabDzivDF5BHmSvBNVxUr.7bnyWzJbiy6eusWr6yPgzIcrX6IGlhWtVhgQ6xpBxs4G4!/b&bo=mgLZAAAAAAADF3M!&rf=viewer_4&t=5)
![image](./images/2.png)


















