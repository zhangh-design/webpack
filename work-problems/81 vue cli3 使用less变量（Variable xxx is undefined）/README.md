### 81 vue cli3 使用 less 变量（Variable xxx is undefined）

<font color=red>下面的问题会出现在 less-loader 的版本小于 6.0.0（我本地电脑上的 vue cli 版本是 @vue/cli 4.5.13 创建的项目中 less-loader 版本是 5.0.0），如果你手动将 less-loader 的版本升级至 > 6.0.0 的话无需安装下面的方法来解决这个问题。</font>

vue cli3 初始化构建项目时，自定义选择使用 less 作为 css 预处理器之后，就会默认安装 less、less-loader，并且直接可以在组件中正常使用嵌套结构书写 css 了。

但是，当我们需要使用 less 变量时，比如下面的情况：

1. 我新建了一个 common.less 文件，并在 main.js 中引入它：

![image](./images/common.png)

2. common.less 中任意定义一个变量，比如：

![image](./images/pink.png)

3. 将组件中任意颜色值，修改为 @pink（对应左侧绿色字体）

![image](./images/test_pink.png)

4. 这时编译报错变量未定义：Variable @pink is undefined

![image](./images/pink_is_undefined.png)

怎么样才能正常使用呢？

5. 安装 style-resources-loader

```
npm i style-resources-loader --save-dev
```

6. 在 vue.config.js（若没有该文件，在根目录下新建）中，增加下面的配置：

patterns 传入你要加载的含 less 变量的文件地址（相对路径）。可以传字符串，也可以传数组

```javascript
const vueConfig = {
  // 加上以下的配置
  chainWebpack: (config) => {
    const oneOfsMap = config.module.rule("less").oneOfs.store;
    oneOfsMap.forEach((item) => {
      item
        .use("style-resources-loader")
        .loader("style-resources-loader")
        .options({
          // or an array : ["./path/to/vars.less", "./path/to/mixins.less"] 这里的路径不能使用@，否则会报错
          patterns: "./src/assets/css/common.less",
        })
        .end();
    });
  },
  // 其他配置...
};
module.exports = vueConfig;
```

7. 然后重启项目 npm run dev，发现可以正常使用了

![image](./images/success.png)

less 变量的情况，我一般用于全局定义几个常用的色值，组件中拿变量使用，这样后期维护起来，只需要去修改定义的地方即可。还是很方便的~
